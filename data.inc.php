<?php
$bitbucket_credentials = array(
    'username' => 'your_username',
    'password' => 'your_password'
);

$projects = array(
	array(
		'git_slug'=>'bitbucket-ftp-deploy',
		'branches'=>array(
			'master'=>array(
				'type'=>'ftp',
				'ftp_host'=>'ftp.example.org',
				'ftp_user'=>'example_username',
				'ftp_pass'=>'example_password',
				'ftp_path'=>'/htdocs/',
			),
			'beta'=>array(
				'type'=>'ftp',
				'ftp_host'=>'ftp.example.org',
				'ftp_user'=>'example_username',
				'ftp_pass'=>'example_password',
				'ftp_path'=>'/htdocs_beta/',
			),
		),
	),
);
?>